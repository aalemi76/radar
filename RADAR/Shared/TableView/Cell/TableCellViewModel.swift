//
//  SharedViewController.swift
//  RADAR
//
//  Created by AliReza on 2022-11-17.
//

import Foundation
class TableCellViewModel: Reusable {
    let reuseID: String
    let cellClass: AnyClass
    var model: Any
    required init(reuseID: String, cellClass: AnyClass, model: Any) {
        self.reuseID = reuseID
        self.cellClass = cellClass
        self.model = model
    }
    func getReuseID() -> String { return reuseID}
    func getCellClass() -> AnyClass { return cellClass }
    func getModel() -> Any { return model }
    func updateModel(model: Any) {
        self.model = model
    }
    func cellDidLoad(_ cell: Updatable) {
        return
    }
}
