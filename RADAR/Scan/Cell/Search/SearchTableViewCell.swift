//
//  SearchTableViewCell.swift
//  RADAR
//
//  Created by AliReza on 2022-11-17.
//

import UIKit

enum SearchState: String {
    case searching = "Stop scan"
    case stop = "Start scan"
}

class SearchTableViewCell: UITableViewCell, Updatable {

    static let reuseID = String(describing: SearchTableViewCell.self)
    
    var viewModel: SearchCellViewModel?
    
    var searchState: SearchState = .stop {
        willSet(newValue) {
            searchButton.setTitle(newValue.rawValue, for: .normal)
            if newValue == .searching {
                searchButton.backgroundColor = GlobalSettings.shared().lightRed
            } else {
                searchButton.backgroundColor = GlobalSettings.shared().lightBlue
            }
        }
    }
    
    @IBOutlet weak var sortButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    
    @IBAction func didTapOnSortButton() {
        viewModel?.didTapOnSortButton.send("")
    }
    
    @IBAction func didTapOnSearchButton() {
        viewModel?.didTapOnSearchButton.send(searchState)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        sortButton.setTitle("", for: .normal)
        searchButton.layer.masksToBounds = true
        searchButton.layer.cornerRadius = 10
    }
    
    func attach(viewModel: Reusable) {
        self.viewModel = viewModel as? SearchCellViewModel
        viewModel.cellDidLoad(self)
    }
    
    func update(model: Any) {
        searchButton.setTitle(searchState.rawValue, for: .normal)
        return
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
