//
//  SharedViewController.swift
//  RADAR
//
//  Created by AliReza on 2022-11-17.
//

import UIKit
protocol Sectionable: AnyObject {
    var title: String? { get }
    init(title: String?, cells: [Reusable], headerView: UIView?, footerView: UIView?)
    func getCells() -> [Reusable]
    func getHeaderView() -> UIView?
    func getFooterView() -> UIView?
    func append(_ cells: [Reusable])
    func removeCells()
}
