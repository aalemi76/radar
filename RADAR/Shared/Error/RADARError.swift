//
//  RADARError.swift
//  RADAR
//
//  Created by AliReza on 2022-11-17.
//

import Foundation

enum RADARError: String, Error {
    case poweredOn = "Bluetooth powered on."
    case unauthorized = "Please go to settings and authorize Bluetooth access for RADAR."
    case poweredOff = "Please turn on your Bluetooth."
    case unKnown = "Please check your Bluetooth settings and try again."
    case scanningFailure = "Could not scan for available Bluetooth devices. Please turn on your Bluetooth and try agian."
    case connectionFailure = "Could not connect to the device."
}
