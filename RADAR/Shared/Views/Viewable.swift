//
//  Viewable.swift
//  RADAR
//
//  Created by AliReza on 2022-11-17.
//

import Foundation

protocol Viewable: AnyObject {
    func show(result: Result<Any, RADARError>)
    func reloadSections(_ sections: [Sectionable])
}
