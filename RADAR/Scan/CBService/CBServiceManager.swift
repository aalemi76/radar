//
//  CBServiceManager.swift
//  RADAR
//
//  Created by AliReza on 2022-11-23.
//

import UIKit
import Combine
import CoreBluetooth

class CBServiceManager: NSObject, CBServiceProvider, CBCentralManagerDelegate {
    
    let centralManager: CBCentralManager
    
    var cbPeripherals = [CBPeripheral]()
    
    var discoveredPeripherals = [Peripheral]() {
        didSet {
            didFindNewPeripheral.send(discoveredPeripherals)
        }
    }
    
    var didFindNewPeripheral = PassthroughSubject<[Peripheral], Never>()
    
    var sendBluetoothConnectionError = PassthroughSubject<RADARError, Never>()
    
    override init() {
        self.centralManager = CBCentralManager()
        super.init()
        self.centralManager.delegate = self
    }
    
    func startScanning() {
        switch centralManager.state {
        case .poweredOn:
            centralManager.scanForPeripherals(withServices: nil)
        case .poweredOff:
            sendBluetoothConnectionError.send(.poweredOff)
        case .unauthorized:
            sendBluetoothConnectionError.send(.unauthorized)
        default:
            sendBluetoothConnectionError.send(.unKnown)
        }
    }
    
    func stopScanning() {
        centralManager.stopScan()
    }
    
    func connect(_ peripheral: Peripheral) {
        
        guard let device = cbPeripherals.filter({ $0.identifier == peripheral.identifier }).first else { return }
        
        centralManager.connect(device)
        
    }
    
    func disconnect(_ peripheral: Peripheral) {
        
        guard let device = cbPeripherals.filter({ $0.identifier == peripheral.identifier }).first else { return }
        
        centralManager.cancelPeripheralConnection(device)
        
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        switch central.state {
        case .poweredOn:
            sendBluetoothConnectionError.send(.poweredOn)
        case .unauthorized:
            sendBluetoothConnectionError.send(.unauthorized)
        case .poweredOff:
            sendBluetoothConnectionError.send(.poweredOff)
            discoveredPeripherals.forEach({ $0.state = .disconnected })
        default:
            sendBluetoothConnectionError.send(.unKnown)
        }
        
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        let identifiers = discoveredPeripherals.map { $0.identifier }
        
        if !identifiers.contains(peripheral.identifier) {
            
            cbPeripherals.append(peripheral)
            
            let NewPeripheral = Peripheral(peripheral)
            NewPeripheral.macData = advertisementData["kCBAdvDataManufacturerData"] as? Data
            discoveredPeripherals.append(NewPeripheral)
        }
        
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        
        guard let connectedDevice = discoveredPeripherals.filter({ $0.identifier == peripheral.identifier }).first else { return }
        
        connectedDevice.state = .connected
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        
        guard let disconnectedDevice = discoveredPeripherals.filter({ $0.identifier == peripheral.identifier }).first else { return }
        
        disconnectedDevice.state = .disconnected
        
    }
    
    func search(for peripheral: Peripheral) -> CBPeripheral? {
        return cbPeripherals.filter({ $0.identifier == peripheral.identifier }).first
    }
    
}
