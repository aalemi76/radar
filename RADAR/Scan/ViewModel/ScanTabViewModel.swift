//
//  ScanTabViewModel.swift
//  RADAR
//
//  Created by AliReza on 2022-11-17.
//

import Foundation
import Combine

enum Sort: String, CaseIterable {
    
    case nameAscending = "Name: Ascending"
    case nameDescending = "Name: Descending"
    case macAscending = "MAC: Ascending"
    case dateDescending = "Date: Descending"
    
}

class ScanTabViewModel: ViewModelProvider {
    
    let manager: CBServiceProvider
    
    weak var view: Viewable?
    
    var sections = [Sectionable]()
    
    var cancellableStorage = Set<AnyCancellable>()
    
    var discoveredPeripherals = [Peripheral]()
    
    var currentSort: Sort = .nameAscending {
        didSet {
            sortSections()
            view?.reloadSections(sections)
        }
    }
    
    init(manager: CBServiceProvider) {
        self.manager = manager
    }
    
    func viewDidLoad(_ view: Viewable) {
        self.view = view
        setupTableViewSections()
        sinkWithCBServiceProvider()
        sinkWithManagerState()
    }
    
    func sinkWithCBServiceProvider() {
        manager.didFindNewPeripheral.sink { [weak self] peripherals in
            self?.addNewDevices(peripherals)
        }.store(in: &cancellableStorage)
    }
    
    func addNewDevices(_ peripherals: [Peripheral]) {
        let peripheralsSet = Set(peripherals)
        let oldDevicesSet = Set(discoveredPeripherals)
        let newDevices = peripheralsSet.subtracting(oldDevicesSet)
        if (newDevices.count != 0) {
            discoveredPeripherals.append(contentsOf: newDevices)
            let cells = newDevices.map { model in
                let viewModel = DeviceCellViewModel(reuseID: DeviceTableViewCell.reuseID, cellClass: DeviceTableViewCell.self, model: model)
                sink(with: viewModel)
                return viewModel
            }
            sections.append(SectionProvider(title: nil, cells: cells, headerView: nil, footerView: nil))
            sortSections()
        }
        view?.reloadSections(sections)
    }
    
    private func setupTableViewSections() {
        
        let searchViewModel = SearchCellViewModel(reuseID: SearchTableViewCell.reuseID, cellClass: SearchTableViewCell.self, model: "")
        sink(with: searchViewModel)
        
        let searchCells = [searchViewModel]
        
        let searchSection = SectionProvider(title: nil, cells: searchCells, headerView: nil, footerView: nil)
        
        sections = [searchSection]
        
        sections.append(SectionProvider(title: nil, cells: [], headerView: nil, footerView: nil))
        
        view?.show(result: .success(sections))
        
    }
    
    private func sink(with viewModel: SearchCellViewModel) {
        
        viewModel.didTapOnSearchButton.sink { [weak self] state in
            switch state {
                
            case .searching:
                self?.manager.stopScanning()
                viewModel.setSearchState(.stop)
                (self?.view as? ScanTabViewController)?.setScanViewState(isHidden: true)
            case .stop:
                if (self?.manager as? CBServiceManager)?.centralManager.state == .poweredOn {
                    viewModel.setSearchState(.searching)
                    (self?.view as? ScanTabViewController)?.setScanViewState(isHidden: false)
                }
                self?.manager.startScanning()
            }
        }.store(in: &cancellableStorage)
        
        viewModel.didTapOnSortButton.sink { [weak self] _ in
            (self?.view as? ScanTabViewController)?.showMenu()
        }.store(in: &cancellableStorage)
        
    }
    
    private func sink(with viewModel: DeviceCellViewModel) {
        
        viewModel.didTapOnConnectionStatus.sink { [weak self] peripheral in
            
            switch peripheral.state {
                
            case .connected:
                self?.manager.disconnect(peripheral)
            case .disconnecting:
                self?.manager.disconnect(peripheral)
            case .disconnected:
                self?.manager.connect(peripheral)
            case .connecting:
                self?.manager.connect(peripheral)
            }
            
        }.store(in: &cancellableStorage)
        
    }
    
    private func sinkWithManagerState() {
        let searchCell = sections[0].getCells().first as? SearchCellViewModel
        manager.sendBluetoothConnectionError.sink { [weak self] error in
            if error != .poweredOn {
                searchCell?.setSearchState(.stop)
                (self?.view as? ScanTabViewController)?.setScanViewState(isHidden: true)
            }
            self?.view?.show(result: .failure(error))
        }.store(in: &cancellableStorage)
    }
    
    private func sortSections() {
        switch currentSort {
            
        case .nameAscending:
            sortByName()
        case .macAscending:
            sortByMacAscending()
        case .dateDescending:
            sortByDate()
        case .nameDescending:
            sortByName(isAscending: false)
        }
    }
    
    private func sortByMacAscending() {
        let searchSection = sections.first
        sections.remove(at: 0)
        sections.sort { element1, element2 in
            let address1 = (element1.getCells().first?.getModel() as? Peripheral)?.macAddress ?? "unknown"
            let address2 = (element2.getCells().first?.getModel() as? Peripheral)?.macAddress ?? "unknown"
            return address1 < address2
        }
        
        if let searchSection {
            sections.insert(searchSection, at: 0)
        }
    }
    
    private func sortByDate() {
        let searchSection = sections.first
        sections.remove(at: 0)
        sections.sort { element1, element2 in
            let date1 = (element1.getCells().first?.getModel() as? Peripheral)?.date ?? Date()
            let date2 = (element2.getCells().first?.getModel() as? Peripheral)?.date ?? Date()
            return date1 > date2
        }
        
        if let searchSection {
            sections.insert(searchSection, at: 0)
        }
    }
    
    private func sortByName(isAscending: Bool = true) {
        
        if isAscending {
            let searchSection = sections.first
            sections.remove(at: 0)
            sections.sort { element1, element2 in
                guard let name1 = (element1.getCells().first?.getModel() as? Peripheral)?.name else { return false }
                guard let name2 = (element2.getCells().first?.getModel() as? Peripheral)?.name else { return true }
                return name1.capitalized < name2.capitalized
            }
            
            if let searchSection {
                sections.insert(searchSection, at: 0)
            }
        } else {
            let searchSection = sections.first
            sections.remove(at: 0)
            sections.sort { element1, element2 in
                guard let name1 = (element1.getCells().first?.getModel() as? Peripheral)?.name else { return false }
                guard let name2 = (element2.getCells().first?.getModel() as? Peripheral)?.name else { return true }
                return name1.capitalized > name2.capitalized
            }
            
            if let searchSection {
                sections.insert(searchSection, at: 0)
            }
        }
        
    }
    
}
