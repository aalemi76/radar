//
//  SharedViewController.swift
//  RADAR
//
//  Created by AliReza on 2022-11-17.
//

import Foundation

protocol ViewModelProvider: AnyObject {
    func viewDidLoad(_ view: Viewable)
}
