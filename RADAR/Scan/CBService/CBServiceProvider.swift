//
//  CBServiceProvider.swift
//  RADAR
//
//  Created by AliReza on 2022-11-23.
//

import Foundation
import Combine
import CoreBluetooth

protocol CBServiceProvider: AnyObject {
    var didFindNewPeripheral: PassthroughSubject<[Peripheral], Never> { get set }
    var sendBluetoothConnectionError: PassthroughSubject<RADARError, Never> { get set }
    func startScanning()
    func stopScanning()
    func connect(_ peripheral: Peripheral)
    func disconnect(_ peripheral: Peripheral)
    func search(for peripheral: Peripheral) -> CBPeripheral?
}
