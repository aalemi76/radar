//
//  ScanTabViewController+Ext.swift
//  RADAR
//
//  Created by AliReza on 2022-11-17.
//

import UIKit

// MARK: - Table View extension

extension ScanTabViewController: Viewable {
    
    func show(result: Result<Any, RADARError>) {
        
        DispatchQueue.main.async {[weak self] in
            
            switch result {
                
            case .success(let success):
                
                guard let sections = success as? [Sectionable] else {
                    self?.showErrorBanner(title: RADARError.scanningFailure.rawValue)
                    return
                }
                self?.configureTableView(sections)
                
            case .failure(let failure):
                
                if failure == .poweredOn {
                    self?.showSuccessBanner(title: failure.rawValue)
                } else {
                    self?.showErrorBanner(title: failure.rawValue)
                }
            }
        }
        
    }
    
    func configureTableView(_ sections: [Sectionable]) {
        tableViewContainer.load(sections, dataSourceHandler: nil, delegateHandler: nil)
        sinkWithTapOnCells()
        tableViewContainer.delegateHandler.scrollViewDidScroll.sink { [weak self] scrollView in
            self?.scrollViewDidScroll(scrollView)
        }.store(in: &cancellableStore)
    }
    
    func reloadSections(_ sections: [Sectionable]) {
        tableViewContainer.reloadSections(sections)
    }
    
    func sinkWithTapOnCells() {
        tableViewContainer.delegateHandler.passSelectedItem.sink { [weak self] model in
            guard let model = model as? Peripheral else { return }
            guard let peripheral = self?.viewModel.manager.search(for: model) else { return }
            
            switch model.state {
                
            case .connected:
                self?.showDeviceDetails.send((peripheral, model))
                
            default:
                return
            }
            
        }.store(in: &cancellableStore)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView?) {
        guard let scrollView = scrollView else { return }
        headerView.scrollViewDidScroll(scrollView: scrollView)
    }
    
}
