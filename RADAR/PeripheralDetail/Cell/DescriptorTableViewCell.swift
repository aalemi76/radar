//
//  DescriptorTableViewCell.swift
//  RADAR
//
//  Created by AliReza on 2022-11-22.
//

import UIKit
import CoreBluetooth

class DescriptorTableViewCell: UITableViewCell, Updatable {
    
    static let reuseID = String(describing: DescriptorTableViewCell.self)
    
    @IBOutlet weak var uuid: UILabel!
    @IBOutlet weak var value: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func attach(viewModel: Reusable) {
        return
    }
    
    func update(model: Any) {
        guard let model = model as? CBDescriptor else { return }
        
        uuid.text = "\(model.uuid)"
        
        if let data = model.value as? Data {
            value.text = String(data: data, encoding: .utf8)
        } else {
            value.text = "-"
        }
        
        return
    }
    
}
