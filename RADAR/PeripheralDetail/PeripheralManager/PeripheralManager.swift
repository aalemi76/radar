//
//  PeripheralManager.swift
//  RADAR
//
//  Created by AliReza on 2022-11-21.
//

import Foundation
import Combine
import CoreBluetooth

class PeripheralManager: NSObject, CBPeripheralDelegate {
    
    enum Property: String {
        case identifier = "Identifier"
        case name = "Name"
        case description = "Description"
        case connection = "Connection"
        case macAddress = "MAC address"
        case date = "Date Scanned"
    }
    
    var didDiscoverService = PassthroughSubject<(String, Bool), Never>()
    var didDiscoverCharacteristic = PassthroughSubject<CBCharacteristic, Never>()
    var didDiscoverDescriptor = PassthroughSubject<CBDescriptor, Never>()
     
    let peripheral: CBPeripheral
    let model: Peripheral
    
    var services = [CBService]()
    var characteristics = [CBCharacteristic]()
    var descriptors = [CBDescriptor]()
    
    let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm E, d MMM y"
        return formatter
    }()
    
    init(peripheral: CBPeripheral, model: Peripheral) {
        self.peripheral = peripheral
        self.model = model
        super.init()
        self.peripheral.delegate = self
        peripheral.discoverServices(nil)
    }
    
    func getPeripheralProperties(_ property: Property) -> (String, String) {
        
        switch property {
            
        case .identifier:
            return (property.rawValue, peripheral.identifier.uuidString)
        case .name:
            return (property.rawValue, peripheral.name ?? "Unknown")
        case .description:
            return (property.rawValue, peripheral.description)
        case .connection:
            return (property.rawValue, model.state.rawValue)
        case .macAddress:
            return (property.rawValue, model.macAddress ?? "Unknown")
        case .date:
            return (property.rawValue, dateFormatter.string(from: model.date))
        }
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        
        guard let services = peripheral.services else { return }
        
        for service in services {
            if !self.services.contains(service) {
                peripheral.discoverCharacteristics(nil, for: service)
                let name = "\(service.uuid)"
                didDiscoverService.send((name, service.isPrimary))
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        
        guard let characteristics = service.characteristics else { return }

          for characteristic in characteristics {
              if !self.characteristics.contains(characteristic) {
                  peripheral.discoverDescriptors(for: characteristic)
                  didDiscoverCharacteristic.send(characteristic)
              }
          }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverDescriptorsFor characteristic: CBCharacteristic, error: Error?) {
        
        guard let descriptors = characteristic.descriptors else { return }
        
        for descriptor in descriptors {
            if !self.descriptors.contains(descriptor) {
                didDiscoverDescriptor.send(descriptor)
            }
            
        }
        
        peripheral.writeValue(Data(), for: characteristic, type: .withResponse)
    }
    
}
