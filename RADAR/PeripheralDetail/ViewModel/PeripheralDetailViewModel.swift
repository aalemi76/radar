//
//  PeripheralDetailViewModel.swift
//  RADAR
//
//  Created by AliReza on 2022-11-20.
//

import Foundation
import Combine

class PeripheralDetailViewModel: ViewModelProvider {
    
    weak var view: Viewable?
    
    let manger: PeripheralManager
    
    var sections = [Sectionable]()
    
    var cancellableStore = Set<AnyCancellable>()
    
    init(manger: PeripheralManager) {
        self.manger = manger
    }
    
    func viewDidLoad(_ view: Viewable) {
        self.view = view
        setupTableView()
        addNewService()
        addNewCharacteristic()
        addNewDescriptor()
    }
    
    private func setupTableView() {
        
        var cells = [Reusable]()
        
        let idCell = TableCellViewModel(reuseID: DetailTableViewCell.reuseID, cellClass: DetailTableViewCell.self, model: manger.getPeripheralProperties(.identifier))
        cells.append(idCell)
        let nameCell = TableCellViewModel(reuseID: DetailTableViewCell.reuseID, cellClass: DetailTableViewCell.self, model: manger.getPeripheralProperties(.name))
        cells.append(nameCell)
        let statusCell = TableCellViewModel(reuseID: DetailTableViewCell.reuseID, cellClass: DetailTableViewCell.self, model: manger.getPeripheralProperties(.connection))
        cells.append(statusCell)
        let macCell = TableCellViewModel(reuseID: DetailTableViewCell.reuseID, cellClass: DetailTableViewCell.self, model: manger.getPeripheralProperties(.macAddress))
        cells.append(macCell)
        let dateCell = TableCellViewModel(reuseID: DetailTableViewCell.reuseID, cellClass: DetailTableViewCell.self, model: manger.getPeripheralProperties(.date))
        cells.append(dateCell)
        
        let section = SectionProvider(title: "Genral Properties", cells: cells, headerView: nil, footerView: nil)
        
        sections.append(section)
        
        sections.append(SectionProvider(title: "Service Properties", cells: [], headerView: nil, footerView: nil))
        
        sections.append(SectionProvider(title: "Characteristic Properties", cells: [], headerView: nil, footerView: nil))
        
        sections.append(SectionProvider(title: "Descriptor Properties", cells: [], headerView: nil, footerView: nil))
        
        view?.show(result: .success(sections))
    }
    
    private func addNewService() {
        
        let serviceSection = sections[1]
        
        manger.didDiscoverService.sink { [weak self] name, isPrimary in
            let cell = TableCellViewModel(reuseID: ServiceTableViewCell.reuseID, cellClass: ServiceTableViewCell.self, model: (name, isPrimary))
            serviceSection.append([cell])
            self?.view?.reloadSections(self?.sections ?? [])
        }.store(in: &cancellableStore)
    }
    
    private func addNewCharacteristic() {
        
        let charSection = sections[2]
        
        manger.didDiscoverCharacteristic.sink { [weak self] character in
            let cell = TableCellViewModel(reuseID: CharacteristicTableViewCell.reuseID, cellClass: CharacteristicTableViewCell.self, model: character)
            charSection.append([cell])
            self?.view?.reloadSections(self?.sections ?? [])
        }.store(in: &cancellableStore)
    }
    
    private func addNewDescriptor() {
          
        let descriptorSection = sections[3]
        
        manger.didDiscoverDescriptor.sink { [weak self] descriptor in
            let cell = TableCellViewModel(reuseID: DescriptorTableViewCell.reuseID, cellClass: DescriptorTableViewCell.self, model: descriptor)
            descriptorSection.append([cell])
            self?.view?.reloadSections(self?.sections ?? [])
        }.store(in: &cancellableStore)
    }
}
