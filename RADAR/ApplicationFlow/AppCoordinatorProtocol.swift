//
//  AppCoordinatorProtocol.swift
//  RADAR
//
//  Created by AliReza on 2022-11-17.
//

import Foundation

protocol AppCoordinatorProtocol: Coordinator {
    func showLoginFlow()
    func showMainFlow()
}
