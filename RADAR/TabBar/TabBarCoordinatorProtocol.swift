//
//  TabBarCoordinatorProtocol.swift
//  RADAR
//
//  Created by AliReza on 2022-11-17.
//

import UIKit

protocol TabBarCoordinatorProtocol: Coordinator {
    
    var tabBarController: UITabBarController { get set }
    
}
