//
//  PeripheralDetailViewController.swift
//  RADAR
//
//  Created by AliReza on 2022-11-20.
//

import UIKit
import CoreBluetooth

class PeripheralDetailViewController: SharedViewController {
    
    let viewModel: PeripheralDetailViewModel
    
    private lazy var tableView: UITableView = {
        let tbl = UITableView(frame: .zero, style: .insetGrouped)
        tbl.backgroundColor = GlobalSettings.shared().mainColor
        tbl.tableHeaderView = nil
        tbl.tableFooterView = nil
        tbl.sectionHeaderHeight = 44
        tbl.sectionFooterHeight = 0
        tbl.separatorColor = GlobalSettings.shared().lightBlue
        tbl.rowHeight = UITableView.automaticDimension
        tbl.estimatedRowHeight = 122
        tbl.separatorColor = GlobalSettings.shared().mainColor
        return tbl
    }()
    
    lazy var tableViewContainer: TableViewContainer = {
        let container = TableViewContainer(tableView: tableView)
        container.backgroundColor = GlobalSettings.shared().mainColor
        return container
    }()
    
    init(viewModel: PeripheralDetailViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad(self)
        addTableViewContainer()
    }
    
    func addTableViewContainer() {
        tableViewContainer.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableViewContainer)
        NSLayoutConstraint.activate([
            tableViewContainer.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableViewContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableViewContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableViewContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    
}
