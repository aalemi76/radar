//
//  RADARNavigationController.swift
//  RADAR
//
//  Created by AliReza on 2022-11-17.
//

import UIKit

class RADARNavigationController: UINavigationController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    private func configureView() {
        let appearance = UINavigationBarAppearance()
        appearance.backgroundColor = GlobalSettings.shared().mainColor
        appearance.titleTextAttributes = [.foregroundColor: GlobalSettings.shared().lightBlue ?? .white]
        appearance.largeTitleTextAttributes = [.foregroundColor: GlobalSettings.shared().lightBlue ?? .white]
        navigationBar.tintColor = .white
        navigationBar.standardAppearance = appearance
        navigationBar.compactAppearance = appearance
        navigationBar.scrollEdgeAppearance = appearance
    }

}
