# RADAR

## Overview
<img src="https://img.shields.io/badge/Platform-iOS-blueviolet">
<img src="https://img.shields.io/badge/Language-Swift-important">
<img src="https://img.shields.io/badge/iOS-13%2B-yellow">


Within this application, the user can scan for all available Bluetooth devices as well as connect and disconnect to all found devices. The user can sort the list of Bluetooth devices by name, mac address, and time scanned. While connected to a device, the user can navigate to device details and see the device's services, characteristics, and descriptors.

<img src="appScreenshot.jpeg" width="332.70" height="720">

## Architecture

The app's architecture is MVVM-C. There is a model called Peripherals, which stores available properties of Bluetooth devices, such as name, state, identifier, and MAC address. There is a Bluetooth service provider called CBServiceManger which scans for peripherals (Bluetooth devices) and stores them in an array. It can also connect to and disconnect from a peripheral. ViewModel owns this class instance and observes its changes to update the coresponding view controller. ViewControllers only display UI components and receive user interaction and pass it to ViewModel. The coordinator in this app takes care of navigating between different parts of the applications. They create ViewControllers and also observe them if needed. There is also a PeripheralManager class which uses to investigate the device's services, characteristics, and descriptors.


