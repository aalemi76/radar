//
//  AppCoordinator.swift
//  RADAR
//
//  Created by AliReza on 2022-11-17.
//

import UIKit
import Combine

class AppCoordinator: AppCoordinatorProtocol {
    
    var didFinishCoordinator: PassthroughSubject<Coordinator, Never>
    
    var childCoordinators: [Coordinator]
    
    var navigationController: UINavigationController
    
    var type: CoordinatorType
    
    var cancellableStorage = Set<AnyCancellable>()
    
    required init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.childCoordinators = []
        self.type = .app
        self.didFinishCoordinator = PassthroughSubject<Coordinator, Never>()
        self.navigationController.setNavigationBarHidden(true, animated: true)
    }
    
    func launch() {
        showMainFlow()
    }
    
    func showLoginFlow() {
        return
    }
    
    func showMainFlow() {
        
        let tabBarCoordinator = TabBarCoordinator(navigationController: navigationController)
        
        tabBarCoordinator.didFinishCoordinator.sink { [weak self] coordinator in
            
            guard let self = self else { return }
            
            self.childCoordinators = self.childCoordinators.filter({ $0.type != coordinator.type })
            
            switch coordinator.type {
                
            case .tab:
                self.navigationController.viewControllers.removeAll()
                self.showLoginFlow()
                
            default:
                break
            }
            
        }.store(in: &cancellableStorage)
        
        tabBarCoordinator.launch()
        
        childCoordinators.append(tabBarCoordinator)
        
        return
    }
    
}
