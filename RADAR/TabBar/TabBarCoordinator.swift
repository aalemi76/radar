//
//  TabBarCoordinator.swift
//  RADAR
//
//  Created by AliReza on 2022-11-17.
//

import UIKit
import Combine

enum TabType {
    
    case scan
    
}


class TabBarCoordinator: TabBarCoordinatorProtocol {
    
    var tabBarController: UITabBarController
    
    var didFinishCoordinator: PassthroughSubject<Coordinator, Never>
    
    var childCoordinators: [Coordinator]
    
    var navigationController: UINavigationController
    
    var type: CoordinatorType
    
    required init(navigationController: UINavigationController) {
        self.tabBarController = UITabBarController()
        self.didFinishCoordinator = PassthroughSubject<Coordinator, Never>()
        self.childCoordinators = []
        self.navigationController = navigationController
        self.type = .tab
    }
    
    func launch() {
        
        createTab(with: .scan)
        
        createTabBar(childCoordinators.map({ $0.navigationController }))
        
    }
    
    private func createTab(with tabType: TabType) {
        
        let navigationController = RADARNavigationController()
        
        switch tabType {
            
        case .scan:
            navigationController.tabBarItem = UITabBarItem(title: "Scan", image: UIImage(systemName: "iphone.gen1.radiowaves.left.and.right.circle"), selectedImage: UIImage(systemName: "iphone.gen1.radiowaves.left.and.right.circle.fill"))
            let coordinator = ScanTabCoordinator(navigationController: navigationController)
            coordinator.launch()
            childCoordinators.append(coordinator)
            
        }
        
    }
    
    private func createTabBar(_ viewControllers: [UIViewController]) {
        
        tabBarController.setViewControllers(viewControllers, animated: true)
        tabBarController.selectedIndex = 0
        
        if #available(iOS 15, *) {
            
            let appearance = UITabBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = GlobalSettings.shared().lightBlue
            tabBarController.tabBar.standardAppearance = appearance
            tabBarController.tabBar.scrollEdgeAppearance = tabBarController.tabBar.standardAppearance
            
        } else {
            tabBarController.tabBar.isTranslucent = false
            tabBarController.view.backgroundColor = GlobalSettings.shared().mainColor
            tabBarController.tabBar.backgroundColor = GlobalSettings.shared().lightBlue
            tabBarController.tabBar.tintColor = GlobalSettings.shared().mainColor
        }
        tabBarController.tabBar.tintColor = GlobalSettings.shared().mainColor
        tabBarController.tabBar.unselectedItemTintColor = .white
        tabBarController.tabBar.layer.masksToBounds = true
        tabBarController.tabBar.layer.cornerRadius = 20
        tabBarController.tabBar.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        navigationController.viewControllers = [tabBarController]
    }
    
    
}
