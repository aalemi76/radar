//
//  DetailTableViewCell.swift
//  RADAR
//
//  Created by AliReza on 2022-11-20.
//

import UIKit

class DetailTableViewCell: UITableViewCell, Updatable {
    
    static let reuseID =  String(describing: DetailTableViewCell.self)
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var content: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func attach(viewModel: Reusable) {
        return
    }
    
    func update(model: Any) {
        
        guard let model = model as? (String, String) else { return }
        titleLabel.text = model.0 + ":"
        content.text = model.1
    }
    
}
