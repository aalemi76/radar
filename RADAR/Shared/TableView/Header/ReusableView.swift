//
//  SharedViewController.swift
//  RADAR
//
//  Created by AliReza on 2022-11-17.
//

import Foundation
protocol ReusableView {
    func getReuseID() -> String
    func getViewClass() -> AnyClass
}
