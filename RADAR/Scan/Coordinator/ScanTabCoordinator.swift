//
//  ScanTabCoordinator.swift
//  RADAR
//
//  Created by AliReza on 2022-11-17.
//

import UIKit
import Combine
import CoreBluetooth

class ScanTabCoordinator: Coordinator {
    
    var didFinishCoordinator: PassthroughSubject<Coordinator, Never>
    
    var childCoordinators: [Coordinator]
    
    var navigationController: UINavigationController
    
    var type: CoordinatorType
    
    var cancellableStorage = Set<AnyCancellable>()
    
    required init(navigationController: UINavigationController) {
        self.didFinishCoordinator = PassthroughSubject()
        self.childCoordinators = []
        self.navigationController = navigationController
        self.type = .tab
    }
    
    func launch() {
        let viewModel = ScanTabViewModel(manager: CBServiceManager())
        let viewController = ScanTabViewController(viewModel: viewModel)
        viewController.title = "Scan"
        viewController.showDeviceDetails.sink { [weak self] (peripheral, model) in
            self?.showDetailViewController(peripheral: peripheral, model: model)
        }.store(in: &cancellableStorage)
        push(viewController: viewController, animated: true)
    }
    
    func showDetailViewController(peripheral: CBPeripheral, model: Peripheral) {
        let manager = PeripheralManager(peripheral: peripheral, model: model)
        let viewModel = PeripheralDetailViewModel(manger: manager)
        let viewController = PeripheralDetailViewController(viewModel: viewModel)
        viewController.title = "Detail"
        push(viewController: viewController, animated: true)
    }
}
