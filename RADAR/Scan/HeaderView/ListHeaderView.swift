//
//  ListHeaderView.swift
//  RADAR
//
//  Created by AliReza on 2022-11-18.
//

import UIKit

class ListHeaderView: UIView {
    
    let viewHeight: CGFloat
    let imageName: String
    
    private lazy var animator = UIViewPropertyAnimator(duration: 3, curve: .easeIn)
    
    let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .regular))
    
    private var imageViewHeight = NSLayoutConstraint()
    private var imageViewBottom = NSLayoutConstraint()
    
    private var containerViewHeight = NSLayoutConstraint()
    
    private var containerView = UIView()
    
    private lazy var imageView: UIImageView = {
        let img = UIImageView(image: UIImage(named: imageName))
        img.contentMode = .scaleAspectFill
        img.clipsToBounds = true
        return img
    }()
    
    private lazy var scanView: UIView = {
        let view = UIView()
        view.backgroundColor = .none
        return view
    }()
    
    private lazy var scanImage: UIImageView = {
        let image = UIImageView(image: UIImage(systemName: "magnifyingglass.circle.fill"))
        image.tintColor = GlobalSettings.shared().lightBlue
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    private lazy var pulseLayer: CAShapeLayer = {
        let layer = CAShapeLayer()
        return layer
    }()
    
    init(viewHeight: Double, frame: CGRect, imageName: String) {
        
        self.viewHeight = CGFloat(viewHeight)
        self.imageName = imageName
        super.init(frame: frame)
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        
        self.viewHeight = 0
        self.imageName = ""
        super.init(coder: coder)
        setConstraints()
        fatalError("init(coder:) has not been implemented")
        
    }
    
    func setConstraints() {
        
        addSubview(containerView)
        containerView.addSubview(imageView)
        
//         UIView Constraints
        NSLayoutConstraint.activate([
            containerView.widthAnchor.constraint(equalTo: widthAnchor),
            self.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            containerView.heightAnchor.constraint(equalTo: heightAnchor)
        ])
        
        // Container View Constraints
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        imageView.widthAnchor.constraint(equalTo: containerView.widthAnchor).isActive = true
        containerViewHeight = containerView.heightAnchor.constraint(equalTo: self.heightAnchor)
        containerViewHeight.isActive = true
        
        // ImageView Constraints
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageViewBottom = imageView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
        imageViewBottom.isActive = true
        imageViewHeight = imageView.heightAnchor.constraint(equalTo: containerView.heightAnchor)
        imageViewHeight.isActive = true
        
        addScanView()
        
    }
    
    private func addScanView() {
        scanView.alpha = 0
        // Scan image
        scanView.translatesAutoresizingMaskIntoConstraints = false
        scanView.addSubview(scanImage)
        scanImage.pinToEdge(scanView)
        
        scanView.translatesAutoresizingMaskIntoConstraints = false
        imageView.addSubview(scanView)
        NSLayoutConstraint.activate([
            scanView.heightAnchor.constraint(equalToConstant: 60),
            scanView.widthAnchor.constraint(equalToConstant: 60),
            scanView.centerYAnchor.constraint(equalTo: imageView.centerYAnchor, constant: -20),
            scanView.trailingAnchor.constraint(equalTo: imageView.trailingAnchor, constant: -30)
        ])
        
        scanView.layoutSubviews()
        
    }
    
    func setupLayer() {
        
        let path = getPath(viewHeight: Double(viewHeight))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = path.cgPath
        imageView.layer.mask = maskLayer
        
        let borderLayer = CAShapeLayer()
        borderLayer.path = maskLayer.path
        borderLayer.fillColor = UIColor.clear.cgColor
        imageView.layer.addSublayer(borderLayer)
        addCircularPath()
    }
    
    func addCircularPath() {
        let radius: CGFloat = 30
        let path = UIBezierPath(arcCenter: .zero, radius: radius, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        pulseLayer.path = path.cgPath
        pulseLayer.strokeColor = GlobalSettings.shared().lightBlue?.cgColor
        pulseLayer.lineWidth = 25
        pulseLayer.opacity = 0.7
        pulseLayer.fillColor = .none
        pulseLayer.lineCap = CAShapeLayerLineCap.round
        pulseLayer.position = CGPoint(x: scanView.center.x + radius, y: scanView.center.y + radius)
        scanView.layer.addSublayer(pulseLayer)
    }
    
    func animatePulseLayer() {
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.toValue = 1.4
        animation.duration = 0.8
        animation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        animation.autoreverses = true
        animation.repeatCount = .infinity
        pulseLayer.add(animation, forKey: "pulsing")
    }
    
    func setScanViewVisibility(isHidden: Bool) {
        UIView.animate(withDuration: 0.7, delay: 0, options: .curveEaseInOut) {[weak self] in
            if isHidden {
                self?.scanView.alpha = 0
                self?.pulseLayer.removeAnimation(forKey: "pulsing")
            } else {
                self?.scanView.alpha = 1
                self?.animatePulseLayer()
            }
        }
    }
    
    func restartAnimation() {
        if scanView.alpha != 0 {
            pulseLayer.removeAnimation(forKey: "pulsing")
            animatePulseLayer()
        }
    }
    
    func getPath(viewHeight: Double) -> UIBezierPath {
        
        let path = UIBezierPath(rect: frame)
        path.move(to: CGPoint(x: UIScreen.main.bounds.width, y: 0))
        path.addLine(to: CGPoint(x: 0, y: 0))
        path.addLine(to: CGPoint(x: 0, y: 2*viewHeight/3))
        path.addCurve(to: CGPoint(x: UIScreen.main.bounds.width, y: viewHeight - 50), controlPoint1: CGPoint(x: UIScreen.main.bounds.width/2 - 50, y: 2*viewHeight/3 - 50), controlPoint2: CGPoint(x: UIScreen.main.bounds.width/2 + 50, y: viewHeight))
        path.addLine(to: CGPoint(x: UIScreen.main.bounds.width, y: 0))
        path.close()
        
        return path
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        containerViewHeight.constant = scrollView.contentInset.top
        let offsetY = -(scrollView.contentOffset.y + scrollView.contentInset.top)
        containerView.clipsToBounds = offsetY <= 0
        imageViewBottom.constant = offsetY >= 0 ? 0 : -offsetY / 2
        imageViewHeight.constant = max(offsetY + scrollView.contentInset.top, scrollView.contentInset.top)
        
        
//        if scrollView.contentOffset.y > 0 {
//            animator.fractionComplete = 0
//        } else {
//            animator.fractionComplete = abs(scrollView.contentOffset.y) / 100
//        }
    }
    
    func setupVisualEffects() {
        if #available(iOS 13, *) {
            imageView.addSubview(visualEffectView)
            visualEffectView.pinToEdge(imageView)
            
            animator.addAnimations { [weak self] in
                self?.visualEffectView.effect = nil
            }
            
            animator.isReversed = true
            animator.fractionComplete = 0
            
        } else {
            animator.addAnimations {[weak self] in
                guard let self = self else { return }
                
                let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .regular))
                self.imageView.addSubview(visualEffectView)
                visualEffectView.pinToEdge(self.imageView)
                
            }
        }
    }
}
