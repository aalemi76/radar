//
//  ServiceTableViewCell.swift
//  RADAR
//
//  Created by AliReza on 2022-11-21.
//

import UIKit

class ServiceTableViewCell: UITableViewCell, Updatable {
    
    static let reuseID = String(describing: ServiceTableViewCell.self)
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var isPrimary: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func attach(viewModel: Reusable) {
        return
    }
    
    func update(model: Any) {
        
        guard let model = model as? (String, Bool) else { return }
        name.text = model.0
        if model.1 {
            isPrimary.text = "Yes"
        } else {
            isPrimary.text = "No"
        }
    }
    
}
