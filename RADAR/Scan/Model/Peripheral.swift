//
//  Peripheral.swift
//  RADAR
//
//  Created by AliReza on 2022-11-18.
//

import Foundation
import CoreBluetooth
import Combine

enum ConnectionState: String {
    
    case connected = "connected"
    case connecting = "connecting"
    case disconnected = "disconnected"
    case disconnecting = "disconnecting"
    
}

class Peripheral: Hashable {
    
    var identifier: UUID?
    var name: String = "Unknown"
    var deviceDescription: String?
    var state: ConnectionState {
        didSet {
            didConnectionStateChanges.send(state)
        }
    }
    var macAddress: String?
    var macData: Data? {
        didSet {
            makeMACaddress()
        }
    }
    
    var date: Date
    
    var didConnectionStateChanges = PassthroughSubject<ConnectionState, Never>()
    
    init(identifier: UUID? = nil, name: String? = nil, deviceDescription: String? = nil, state: ConnectionState, macAddress: String? = nil, macData: Data? = nil) {
        self.identifier = identifier
        self.name = name ?? "Unknown"
        self.deviceDescription = deviceDescription
        self.state = state
        self.macAddress = macAddress
        self.macData = macData
        self.date = Date()
    }
    
    init(_ peripheral: CBPeripheral) {
        self.identifier = peripheral.identifier
        self.name = peripheral.name ?? "Unknown"
        self.deviceDescription = peripheral.description
        switch peripheral.state {
            
        case .connected:
            state = .connected
        case .disconnected:
            state = .disconnected
        case .connecting:
            state = .connecting
        case .disconnecting:
            state = .disconnecting
        @unknown default:
            state = .disconnected
        }
        self.date = Date()
    }
    
    func makeMACaddress() {
        guard let data = macData else { return }
        var str = ""
        for num in Array(data) {
            str += "\(String(format:"%02X", num)):"
        }
        while str.count < 18 {
            str += "00:"
        }
        str.removeLast()
        let offset = str.count - 17
        var endIndex = str.index(str.endIndex, offsetBy: -offset)
        if offset <= 0 {
            endIndex = str.endIndex
        }
        str = String(str[str.startIndex..<endIndex])
        macAddress = str
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }
    
    static func == (lhs: Peripheral, rhs: Peripheral) -> Bool {
        return lhs.identifier == rhs.identifier
    }
}
