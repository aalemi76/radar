//
//  SharedViewController.swift
//  RADAR
//
//  Created by AliReza on 2022-11-17.
//

import UIKit
class TableViewContainer: UIView, TableViewProvider {
    var sections: [Sectionable] = []
    var tableView: UITableView
    var dataSourceHandler: TableViewDataSourceHandler
    var delegateHandler: TableViewDelegateHandler
    required init(tableView: UITableView) {
        self.tableView = tableView
        dataSourceHandler = TableViewDataSourceHandler(sections: sections)
        delegateHandler = TableViewDelegateHandler(sections: sections)
        super.init(frame: .zero)
        addSubview(self.tableView)
        self.tableView.pinToEdge(self)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func reloadSections(_ sections: [Sectionable]) {
        self.sections = sections
        registerSections(sections)
        dataSourceHandler.sections = sections
        delegateHandler.sections = sections
        tableView.reloadData()
    }
}
