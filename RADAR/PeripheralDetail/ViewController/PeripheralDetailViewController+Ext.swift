//
//  PeripheralDetailViewController+Ext.swift
//  RADAR
//
//  Created by AliReza on 2022-11-20.
//

import UIKit
import CoreBluetooth

// MARK: - Table View extension

extension PeripheralDetailViewController: Viewable {
    
    func show(result: Result<Any, RADARError>) {
        
        DispatchQueue.main.async {[weak self] in
            
            switch result {
                
            case .success(let success):
                
                guard let sections = success as? [Sectionable] else {
                    self?.showErrorBanner(title: RADARError.scanningFailure.rawValue)
                    return
                }
                self?.configureTableView(sections)
                
            case .failure(let failure):
                
                self?.showErrorBanner(title: failure.rawValue)
            }
        }
        
    }
    
    func configureTableView(_ sections: [Sectionable]) {
        tableViewContainer.load(sections, dataSourceHandler: nil, delegateHandler: nil)
    }
    
    func reloadSections(_ sections: [Sectionable]) {
        tableViewContainer.reloadSections(sections)
    }
}
