//
//  SearchCellViewModel.swift
//  RADAR
//
//  Created by AliReza on 2022-11-17.
//

import Foundation
import Combine

class SearchCellViewModel: TableCellViewModel {
    
    weak var cell: Updatable?
    var didTapOnSearchButton = PassthroughSubject<SearchState, Never>()
    var didTapOnSortButton = PassthroughSubject<String, Never>()
    
    override func cellDidLoad(_ cell: Updatable) {
        self.cell = cell
    }
    
    func setSearchState(_ state: SearchState) {
        (cell as? SearchTableViewCell)?.searchState = state
    }
    
}
