//
//  Coordinator.swift
//  RADAR
//
//  Created by AliReza on 2022-11-17.
//

import UIKit
import Combine

enum CoordinatorType {
    case app
    case tab
}

protocol Coordinator: AnyObject {
    
    var didFinishCoordinator: PassthroughSubject<Coordinator, Never> { get set }
    
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    var type: CoordinatorType { get }
    
    init(navigationController: UINavigationController)
    
    func launch()
    func push(viewController: UIViewController, animated: Bool)
    func pop(animated: Bool)
    func present(viewController: UIViewController, animated: Bool)
    func dimiss(viewController: UIViewController, animated: Bool)
    func end()
    
}


extension Coordinator {
    
    func push(viewController: UIViewController, animated: Bool) {
        navigationController.pushViewController(viewController, animated: animated)
    }
    
    func pop(animated: Bool) {
        navigationController.popViewController(animated: animated)
    }
    
    func present(viewController: UIViewController, animated: Bool) {
        navigationController.present(viewController, animated: true, completion: nil)
    }
    
    func dimiss(viewController: UIViewController, animated: Bool) {
        viewController.dismiss(animated: animated, completion: nil)
    }
    
    func end() {
        childCoordinators.removeAll()
        didFinishCoordinator.send(self)
    }
    
}
