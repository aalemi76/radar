//
//  DeviceTableViewCell.swift
//  RADAR
//
//  Created by AliReza on 2022-11-17.
//

import UIKit

class DeviceTableViewCell: UITableViewCell, Updatable {
    
    static let reuseID = String(describing: DeviceTableViewCell.self)
    
    var state: ConnectionState = .disconnected {
        didSet {
            handleStateChanges()
        }
    }
    
    var viewModel: Reusable?

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var macAddress: UILabel!
    @IBOutlet weak var connectionStatus: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        connectionStatus.layer.masksToBounds = true
        connectionStatus.layer.cornerRadius = 5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func didTapOnConnectionStatus() {
        (viewModel as? DeviceCellViewModel)?.didTapOnConnectionButton(state)
    }
    
    func attach(viewModel: Reusable) {
        self.viewModel = viewModel
        viewModel.cellDidLoad(self)
        return
    }
    
    func update(model: Any) {
        guard let model = model as? Peripheral else { return }
        nameLabel.text = model.name
        macAddress.text = model.macAddress ?? "Unknown"
        state = model.state
        
    }
    
    func handleStateChanges() {
        switch state {
        case .disconnected:
            connectionStatus.setTitle("disconnected", for: .normal)
            connectionStatus.backgroundColor = GlobalSettings.shared().lightRed
            connectionStatus.tintColor = .white
        case .connected:
            connectionStatus.setTitle("connected", for: .normal)
            connectionStatus.backgroundColor = GlobalSettings.shared().lightGreen
            connectionStatus.tintColor = GlobalSettings.shared().blueGreen
        case .connecting:
            connectionStatus.setTitle("connecting...", for: .normal)
            connectionStatus.backgroundColor = GlobalSettings.shared().lightGreen
            connectionStatus.tintColor = GlobalSettings.shared().blueGreen
        case .disconnecting:
            connectionStatus.setTitle("disconnecting...", for: .normal)
            connectionStatus.backgroundColor = GlobalSettings.shared().lightRed
            connectionStatus.tintColor = .white
        }
    }
    
}
