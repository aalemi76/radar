//
//  SharedViewController.swift
//  RADAR
//
//  Created by AliReza on 2022-11-17.
//

import Foundation
protocol Updatable: AnyObject {
    func attach(viewModel: Reusable)
    func update(model: Any)
}
