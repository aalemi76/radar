//
//  ScanTabViewController.swift
//  RADAR
//
//  Created by AliReza on 2022-11-17.
//

import UIKit
import Combine
import CoreBluetooth

class ScanTabViewController: SharedViewController {
    
    var showDeviceDetails = PassthroughSubject<(CBPeripheral, Peripheral), Never>()
    
    let viewModel: ScanTabViewModel
    
    var cancellableStore = Set<AnyCancellable>()
    
    lazy var headerView: ListHeaderView = {
        let view = ListHeaderView(viewHeight: 200, frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 200), imageName: "HeaderView")
        return view
    }()
    
    private lazy var tableView: UITableView = {
        let tbl = UITableView(frame: .zero, style: .insetGrouped)
        tbl.backgroundColor = GlobalSettings.shared().mainColor
        tbl.tableHeaderView = headerView
        tbl.tableFooterView = nil
        tbl.sectionHeaderHeight = 0
        tbl.sectionFooterHeight = 10
        tbl.separatorColor = GlobalSettings.shared().lightBlue
        tbl.rowHeight = UITableView.automaticDimension
        tbl.estimatedRowHeight = 120
        return tbl
    }()
    
    lazy var tableViewContainer: TableViewContainer = {
        let container = TableViewContainer(tableView: tableView)
        container.backgroundColor = GlobalSettings.shared().mainColor
        return container
    }()
    
    init(viewModel: ScanTabViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.tabBarController?.tabBar.isHidden = false
        headerView.restartAnimation()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoad(self)
        addTableViewContainer()
        headerView.setupLayer()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addTableViewContainer() {
        tableViewContainer.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableViewContainer)
        NSLayoutConstraint.activate([
            tableViewContainer.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableViewContainer.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableViewContainer.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableViewContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }
    
    func showMenu() {
        
        var sortOptions = Sort.allCases
        
        let alertVC = UIAlertController(title: "Sort options", message: "Please select a sort option", preferredStyle: .actionSheet)
        
        sortOptions.forEach { [weak self] option in
            if option == viewModel.currentSort {
                alertVC.addAction(UIAlertAction(title: option.rawValue, style: .destructive){ [weak self] action in
                    self?.viewModel.currentSort = option
                })
            } else {
                alertVC.addAction(UIAlertAction(title: option.rawValue, style: .default){ [weak self] action in
                    self?.viewModel.currentSort = option
                })
            }
        }
        
        alertVC.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        
        present(alertVC, animated: true)
        
    }
    
    func setScanViewState(isHidden: Bool) {
        headerView.setScanViewVisibility(isHidden: isHidden)
    }
    
}
