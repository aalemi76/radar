//
//  DeviceCellViewModel.swift
//  RADAR
//
//  Created by AliReza on 2022-11-19.
//

import Foundation
import Combine

class DeviceCellViewModel: TableCellViewModel {
    
    weak var cell: Updatable?
    
    var didTapOnConnectionStatus = PassthroughSubject<Peripheral, Never>()
    
    var cancellableStore = Set<AnyCancellable>()
    
    override func cellDidLoad(_ cell: Updatable) {
        self.cell = cell
        (model as? Peripheral)?.didConnectionStateChanges.sink(receiveValue: { [weak self] newState in
            (self?.cell as? DeviceTableViewCell)?.state = newState
        }).store(in: &cancellableStore)
    }
    
    func didTapOnConnectionButton(_ state: ConnectionState) {
        guard let model = model as? Peripheral else { return }
        switch state {
        case .connected:
            model.state = .disconnecting
        case .disconnected:
            model.state = .connecting
        default:
            break
        }
        didTapOnConnectionStatus.send(model)
    }
}
