//
//  CharacteristicTableViewCell.swift
//  RADAR
//
//  Created by AliReza on 2022-11-22.
//

import UIKit
import CoreBluetooth

class CharacteristicTableViewCell: UITableViewCell, Updatable {
    
    static let reuseID = String(describing: CharacteristicTableViewCell.self)
    
    @IBOutlet weak var uuid: UILabel!
    @IBOutlet weak var properties: UILabel!
    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var notifying: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func attach(viewModel: Reusable) {
        return
    }
    
    func update(model: Any) {
        
        guard let model = model as? CBCharacteristic else { return }
        
        uuid.text = "\(model.uuid)"
        properties.text = "\(model.properties.rawValue)"
        
        if let data = model.value {
            value.text = String(data: data, encoding: .utf8)
        } else {
            value.text = "-"
        }
        
        if model.isNotifying {
            notifying.text = "YES"
        } else {
            notifying.text = "NO"
        }
        return
    }
    
}
